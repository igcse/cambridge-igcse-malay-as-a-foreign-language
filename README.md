# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
This repository is to hold the "Cambridge IGCSE Malay as a Foreign Language" UAT package
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
When delivered to ES Support team, for movement through CPD-204, the content was copied to the directory "clp-uat-repo/Content/UAT/JI/Leckie/CAMBRIDGE-IGCSE-MALAY-SB-29-Mar-V1/". Antony from ES created the directory structure "clp-uat-repo/Content/secondary/igcse/cambridge/cambridge_igcse_malay_as_a_foreign_language" to hold the content from there on.
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines
ANYONE WHO WORK ON THE PACKAGE SHOULD BE USING "clp-uat-repo/Content/secondary/igcse/cambridge/cambridge_igcse_malay_as_a_foreign_language/" at any given point in time. This is to avoid the confusion that may be created when multiple teams are working on the same project (package)

### Version changes ###

* V1 (As received through CPD-181)

1) Content was pointing to " clp-uat-repo/Content/UAT/JI/Leckie/CAMBRIDGE-IGCSE-MALAY-SB-29-Mar-V1/"
* V2 (Uploaded by ES to UAT)
1) Content path was changed from " clp-uat-repo/Content/UAT/JI/Leckie/CAMBRIDGE-IGCSE-MALAY-SB-29-Mar-V1/" to " clp-uat-repo/Content/secondary/igcse/cambridge/cambridge_igcse_malay_as_a_foreign_language/"

### Who do I talk to? ###

* Repo owner or admin
Antony Bineesh (Bineesh K Thomas @ antonybineesh@excelindia.com) from Excel Soft Technologies Pvt. Ltd (Ultimately the property of HarperCollins UK)
* Other community or team contact
Alexander Rutherford (@alexander.rutherford@harpercollins.co.uk)